package com.erang.rangshop.util

import com.erang.rangshop.api.APIClient
import com.erang.rangshop.api.APIInterface

object Common {
    val BASE_URL ="Http://180.64.107.27/rangshop/"
    val api:APIInterface
    get() = APIClient.getClient(BASE_URL).create(APIInterface::class.java)
}