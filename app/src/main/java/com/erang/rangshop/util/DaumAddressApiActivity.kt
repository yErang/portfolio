package com.erang.rangshop.util

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.support.design.widget.FloatingActionButton
import android.webkit.JavascriptInterface
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.widget.TextView
import com.erang.rangshop.R
import kotlinx.android.synthetic.main.activity_daum_address_api.*



class DaumAddressApiActivity : AppCompatActivity() {

    lateinit var webView : WebView
    lateinit var result : TextView
    lateinit var handler: Handler
    lateinit var fabFinish : FloatingActionButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_daum_address_api)

        init()
        init_webView()
        setOnClick()
    }

    fun init(){
        handler = Handler()
        result = daum_result
        fabFinish = daum_finish
    }

    fun init_webView(){
        webView = daum_webview
        webView.settings.javaScriptEnabled = true
        webView.settings.javaScriptCanOpenWindowsAutomatically = true
        webView.addJavascriptInterface(AndroidBridge(), "RangShop")
        webView.webChromeClient = WebChromeClient()
        webView.loadUrl("http://180.64.107.27/rangshop/user/daum_address.php")

    }

    private inner class AndroidBridge{

        @JavascriptInterface
        fun setAddress(arg1: String, arg2: String, arg3: String) {
            handler.post {
                result.text = String.format("(%s) %s %s", arg1, arg2, arg3)
                // WebView를 초기화 하지않으면 재사용할 수 없음
                init_webView()
            }
        }
    }

    fun setOnClick(){
        fabFinish.setOnClickListener {
            val intent = Intent()
            intent.putExtra("result",result.text)
            setResult(Activity.RESULT_OK,intent)
            finish()
        }
    }
}
