package com.erang.rangshop.util

import android.util.Patterns
import android.widget.EditText
import java.text.DecimalFormat
import java.util.regex.Pattern

object TextUtil {
    fun emailValidate(emailText: EditText): Boolean {
        return Patterns.EMAIL_ADDRESS.matcher(emailText.text.toString()).matches()
    }
    fun isEqual(passwordText: EditText, passwordConfirmText: EditText):Boolean{
        return passwordText.text.toString().equals(passwordConfirmText.text.toString())
    }
    fun nameValidate(nameText: EditText):Boolean{
        var patternName: Pattern = Pattern.compile("^[가-힣]{2,10}$")
        return patternName.matcher(nameText.text.toString()).matches()
    }
    fun isEmpty(editText: EditText):Boolean{
        return editText.text.toString().isEmpty()
    }

    fun requestFocus(editText: EditText) : Boolean{
        return editText.requestFocus()
    }

    fun setPrice(priceText: String?) : String? {
        val priceFormat  = DecimalFormat("###,###")
        val formattedPrice = priceFormat.format(priceText?.toInt())

        return formattedPrice
    }
}