package com.erang.rangshop.activity

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.res.ColorStateList
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewCompat
import android.support.v7.app.AlertDialog
import android.util.Log
import android.widget.*
import com.bumptech.glide.Glide
import com.erang.rangshop.R
import com.erang.rangshop.api.response.ProductResponse
import com.erang.rangshop.util.Common
import com.erang.rangshop.util.TextUtil
import kotlinx.android.synthetic.main.activity_product_view.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProductViewActivity : AppCompatActivity() {


    lateinit var productNum : String
    lateinit var productName : TextView
    lateinit var productPrice : TextView
    lateinit var productQuantity : TextView
    lateinit var productDesc : TextView
    lateinit var productImg : ImageView
    lateinit var productCategory : TextView
    lateinit var productPurity : TextView
    var productImgResource : String? = null

    lateinit var appData : SharedPreferences
    lateinit var email : String


    lateinit var btnOrder : Button
    lateinit var btnCart : Button
    lateinit var btnLike : ImageButton
    var isLogin : Boolean = false



    var priceString : String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_view)

        init()
        setOnClick()

    }

    fun init(){
        appData = getSharedPreferences("appData", Context.MODE_PRIVATE)

        isLogin = appData.getBoolean("isLogin",false)
        email = appData.getString("email","")
        productNum = intent.getStringExtra("product_num")

        productName = product_name
        productPrice = product_price
        productQuantity = product_quantity
        productDesc = product_desc
        productImg = product_img
        productCategory = product_category
        productPurity = product_purity
        btnOrder = product_btn_order
        btnLike = product_btn_like
        btnCart = product_btn_cart



    }

    fun setOnClick(){
        btnOrder.setOnClickListener {
           val intent = Intent(this@ProductViewActivity,OrderActivity::class.java)
            startActivity(intent)
        }
        btnLike.setOnClickListener {
            if(isLogin){
                doLike()
            } else {
                Toast.makeText(this@ProductViewActivity,"관심목록 추가는 로그인이 필요합니다",Toast.LENGTH_SHORT).show()
            }
        }

        btnCart.setOnClickListener {
            if(isLogin){
                addCart()
            } else {
                Toast.makeText(this@ProductViewActivity,"구매는 로그인이 필요합니다",Toast.LENGTH_SHORT).show()
            }
        }
    }

    fun doLike(){
        val call = Common.api.doLike(email,productNum)
        call.enqueue(object :Callback<ResponseBody>{
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                val result: String = response.body()!!.string()
                when(result){
                    "like" -> {
                        btnLike.setImageResource(R.drawable.favorite)
                        Toast.makeText(this@ProductViewActivity,"관심목록에 추가되었습니다.",Toast.LENGTH_SHORT).show()
                    }
                    "unlike" -> {
                        btnLike.setImageResource(R.drawable.favorite_border)
                        Toast.makeText(this@ProductViewActivity,"관심목록에서 삭제되었습니다.",Toast.LENGTH_SHORT).show()
                    }
                    "fail" -> {
                        Toast.makeText(this@ProductViewActivity,"오류가 발생했습니다.",Toast.LENGTH_SHORT).show()
                    }

                }
            }
            override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
            }

        })
    }

    fun addCart(){
        val call = Common.api.addCart(email = email, product_num = productNum)
        call.enqueue(object : Callback<ResponseBody>{
            override fun onResponse(call: Call<ResponseBody>?, response: Response<ResponseBody>?) {
                val result: String = response?.body()!!.string()
                when(result){
                    "add" ->{
                        Toast.makeText(this@ProductViewActivity,"장바구니에 추가되었습니다.",Toast.LENGTH_SHORT).show()
                    }
                    "already"->{
                        Toast.makeText(this@ProductViewActivity,"이미 장바구니에 추가된 상품입니다.",Toast.LENGTH_SHORT).show()
                    }
                    "fail"->{
                        Toast.makeText(this@ProductViewActivity,"오류가 발생했습니다.",Toast.LENGTH_SHORT).show()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
            }

        })

    }

    fun getData(){
        val call = Common.api.getProduct(product_num = productNum, isLogin = isLogin , email = email)
        call.enqueue(object : Callback<List<ProductResponse>> {
            override fun onResponse(call: Call<List<ProductResponse>>, response: Response<List<ProductResponse>>) {

                Glide.with(this@ProductViewActivity).load(response.body()?.get(0)?.img).thumbnail(0.1f).into(productImg)
                productImgResource = response.body()?.get(0)?.img
                productName.text = response.body()?.get(0)?.name
                productPrice.text = TextUtil.setPrice(response.body()?.get(0)?.price)+"원"
                priceString = response.body()?.get(0)?.price
                productQuantity.text = response.body()?.get(0)?.quantity
                productDesc.text = response.body()?.get(0)?.desc
                productPurity.text = response.body()?.get(0)?.purity
                productCategory.text = response.body()?.get(0)?.category

                if(isLogin){
                    if (response.body()?.get(1)?.liked.toString() == "ok"){
                        btnLike.setImageResource(R.drawable.favorite)
                    } else {
                        btnLike.setImageResource(R.drawable.favorite_border)
                    }
                }

                if(productQuantity.text=="0"){
                    btnOrder.setText("품절 상품입니다.")
                    btnOrder.isClickable = false
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        ViewCompat.setBackgroundTintList(btnOrder, ColorStateList.valueOf(getColor(R.color.colorImageBackground)))
                    }
                }


            }
            override fun onFailure(call: Call<List<ProductResponse>>, t: Throwable) {
                Log.d("test",t.message)
            }
        })
    }

    override fun onResume() {
        super.onResume()
        getData()
    }
}
