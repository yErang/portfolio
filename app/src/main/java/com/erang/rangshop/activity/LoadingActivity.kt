package com.erang.rangshop.activity

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.ProgressBar

import com.erang.rangshop.R
import kotlinx.android.synthetic.main.activity_loading.*


class LoadingActivity : AppCompatActivity() {


    private lateinit var splashProgressbar : ProgressBar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_loading)

        init()

        Thread(Runnable {
             run{
                doWork()
            }
        }).start()
    }

    private fun init(){
        splashProgressbar = loading_progress
    }

    private fun doWork(){
        for(i in 0..10){
            try {
                Thread.sleep(100)
                splashProgressbar.setProgress(i)
            }catch(e : Exception) {
                e.printStackTrace()
            }
        }
        val intentMain = Intent(SplashActivity@this,MainActivity::class.java)
        startActivity(intentMain)
        finish()
    }
}
