package com.erang.rangshop.activity

import android.app.Activity
import android.app.Fragment
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import com.erang.rangshop.util.BackPressedHandler
import com.erang.rangshop.R
import com.erang.rangshop.fragment.*
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    lateinit var backPressedHandler : BackPressedHandler
    lateinit var bottomNavigationView : BottomNavigationView
    lateinit var appData : SharedPreferences


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()

        bottomNavigationView.setOnNavigationItemSelectedListener { menu ->
            var isLogin= appData.getBoolean("isLogin",false)
            when(menu.itemId){
                R.id.shopping -> {loadFragment(SellingFragment()) }
                R.id.likes -> {
                    if(isLogin){
                        loadFragment(LikesFragment())
                    }else {
                        loadFragment(NotLoginFragment())
                        val intent = Intent(this@MainActivity,LoginActivity::class.java)
                        startActivityForResult(intent, 1)
                        return@setOnNavigationItemSelectedListener true
                    }
                }
                R.id.cart -> {
                    if(isLogin){
                        loadFragment(CartFragment())
                    }else {
                        loadFragment(NotLoginFragment())
                        val intent = Intent(this@MainActivity,LoginActivity::class.java)
                        startActivityForResult(intent, 2)
                        return@setOnNavigationItemSelectedListener true
                    }
                }
                R.id.mypage -> {
                    if(isLogin){
                        loadFragment(MyPageFragment())
                    }else{
                        loadFragment(NotLoginFragment())
                        val intent = Intent(this@MainActivity,LoginActivity::class.java)
                        startActivityForResult(intent, 3)
                        return@setOnNavigationItemSelectedListener true
                    }
                }
                else -> {
                    return@setOnNavigationItemSelectedListener false
                }
            }
        }
    }
    private fun init() {
        backPressedHandler = BackPressedHandler(this)
        bottomNavigationView = main_bottom_nav
        appData = getSharedPreferences("appData",Context.MODE_PRIVATE)
        loadFragment(SellingFragment())
    }

    private fun loadFragment(fragment : Fragment): Boolean {
        fragmentManager.beginTransaction().replace(R.id.main_content,fragment).commit()
        return true
    }

    override fun onBackPressed() {
        backPressedHandler.onBackPressed()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode== Activity.RESULT_OK){
            if(requestCode ==1){
                loadFragment(LikesFragment())
            } else if (requestCode ==2){
                loadFragment(CartFragment())
            } else if (requestCode ==3){
                loadFragment(MyPageFragment())
            }
        }
    }



}
