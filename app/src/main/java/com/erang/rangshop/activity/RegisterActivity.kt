package com.erang.rangshop.activity


import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.erang.rangshop.api.APIInterface
import com.erang.rangshop.R
import com.erang.rangshop.util.Common
import com.erang.rangshop.util.CustomEditText
import com.erang.rangshop.util.DaumAddressApiActivity
import com.erang.rangshop.util.TextUtil
import kotlinx.android.synthetic.main.activity_register.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class RegisterActivity : AppCompatActivity() {

    lateinit var textEmailInput : EditText
    lateinit var textNameInput : EditText
    lateinit var textPasswordInput : EditText
    lateinit var textPasswordconfirmInput : EditText
    lateinit var textAddress : CustomEditText
    lateinit var textAddressDetail : EditText
    lateinit var totalAddress : String
    lateinit var btnRegisterFinish : Button
    lateinit var apiInterface : APIInterface


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        init()
        setOnClick()

    }
    private fun init(){
        textEmailInput = register_email_input
        textNameInput = register_name_input
        textPasswordInput = register_password_input
        textPasswordconfirmInput = register_passwordconfirm_input
        textAddress = register_address_input
        textAddressDetail = register_addressdetail_input
        btnRegisterFinish = register_finish_btn
        apiInterface = Common.api

    }

    private fun register(){
        val call:Call<ResponseBody> = apiInterface.register(textEmailInput.text.toString(), textNameInput.text.toString(), textPasswordInput.text.toString(), textAddress.text.toString(), textAddressDetail.text.toString())
        call.enqueue(object: Callback<ResponseBody>{
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                val result: String = response.body()!!.string()
                when(result){
                    "exist" ->{
                        Toast.makeText(this@RegisterActivity,"이미 사용중인 이메일 주소입니다.",Toast.LENGTH_SHORT).show()
                        textEmailInput.setText("")
                        textEmailInput.requestFocus()
                    }
                    "fail"->{
                        Toast.makeText(this@RegisterActivity,"오류가 발생했습니다.",Toast.LENGTH_SHORT).show()
                    }
                    "success"->{
                        Toast.makeText(this@RegisterActivity,"회원가입이 완료되었습니다.",Toast.LENGTH_SHORT).show()
                        finish()
                    }
                }
            }
            override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
                Toast.makeText(this@RegisterActivity,t!!.message,Toast.LENGTH_SHORT).show()
            }
        })
    }


    fun setOnClick(){
        textAddress.setOnTouchListener(object : View.OnTouchListener{
            override fun onTouch(view: View?, event: MotionEvent?): Boolean {
                if(event?.action == MotionEvent.ACTION_DOWN){
                    val intent = Intent(this@RegisterActivity, DaumAddressApiActivity::class.java)
                    startActivityForResult(intent,1)
                }
                return false
            }
        })

        btnRegisterFinish.setOnClickListener{
            if (TextUtil.isEmpty(textEmailInput)||TextUtil.isEmpty(textNameInput)||TextUtil.isEmpty(textPasswordInput)||TextUtil.isEmpty(textPasswordconfirmInput)
                    ||TextUtil.isEmpty(textAddress)||TextUtil.isEmpty(textAddressDetail)){
                Toast.makeText(this@RegisterActivity,"빈 칸을 입력 해 주세요.",Toast.LENGTH_SHORT).show()

                when {
                    TextUtil.isEmpty(textEmailInput) -> TextUtil.requestFocus(textEmailInput)
                    TextUtil.isEmpty(textNameInput) -> TextUtil.requestFocus(textNameInput)
                    TextUtil.isEmpty(textPasswordInput) -> TextUtil.requestFocus(textPasswordInput)
                    TextUtil.isEmpty(textPasswordconfirmInput) -> TextUtil.requestFocus(textPasswordconfirmInput)
                    TextUtil.isEmpty(textAddress) -> TextUtil.requestFocus(textAddress)
                    TextUtil.isEmpty(textAddressDetail) -> TextUtil.requestFocus(textAddressDetail)
                }

            }else if(!TextUtil.emailValidate(textEmailInput)){
                Toast.makeText(this@RegisterActivity,"올바른 이메일 형식이 아닙니다.",Toast.LENGTH_SHORT).show()
            }else if(!TextUtil.nameValidate(textNameInput)){
                Toast.makeText(this@RegisterActivity,"이름 형식이 올바르지 않습니다",Toast.LENGTH_SHORT).show()
            }else if(!TextUtil.isEqual(textPasswordInput,textPasswordconfirmInput)){
                Toast.makeText(this@RegisterActivity,"비밀번호가 일치하지 않습니다.",Toast.LENGTH_SHORT).show()
            }else {
                totalAddress = textAddress.text.toString()+" "+textAddressDetail.text.toString()
                register()
            }
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        super.onActivityResult(requestCode, resultCode, intent)
        if (requestCode ==1){
            if(resultCode == Activity.RESULT_OK){
                val extras : Bundle = intent?.extras!!
                if(extras !=null){
                    textAddress.setText(extras.getString("result"))
                }
            }
        }
    }



}
