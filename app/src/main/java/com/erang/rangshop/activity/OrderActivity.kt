package com.erang.rangshop.activity

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.MotionEvent
import android.view.View
import android.widget.CheckBox
import android.widget.CompoundButton
import android.widget.EditText
import android.widget.TextView
import com.erang.rangshop.R
import com.erang.rangshop.adpater.OrderAdapter
import com.erang.rangshop.api.response.CartListResponse
import com.erang.rangshop.api.response.OrderListResponse
import com.erang.rangshop.util.Common
import com.erang.rangshop.util.CustomEditText
import com.erang.rangshop.util.DaumAddressApiActivity
import com.erang.rangshop.util.TextUtil
import kotlinx.android.synthetic.main.activity_order.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class OrderActivity : AppCompatActivity() {

    lateinit var checkBox_Address : CheckBox
    lateinit var orderlistRecyclerView: RecyclerView
    lateinit var orderList : List<CartListResponse>

    lateinit var orderAddressMember : TextView
    lateinit var orderAddressAnother : CustomEditText
    lateinit var orderAddressAnotherDetail : EditText
    lateinit var orderTotalPrice : TextView

    lateinit var appData : SharedPreferences



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order)

        init()
        setOnClick()
        getOrderList()
    }

    fun init(){
        orderList = ArrayList()
        checkBox_Address = order_address_checkbox
        orderlistRecyclerView = order_list
        orderlistRecyclerView.layoutManager = LinearLayoutManager(this@OrderActivity)
        appData = getSharedPreferences("appData", Context.MODE_PRIVATE)
        orderAddressMember = order_address_member
        orderAddressMember.setText(appData.getString("address","") + " " + appData.getString("address_detail",""))
        orderAddressAnother = another_address
        orderAddressAnotherDetail = another_detail
        orderTotalPrice = order_totalprice

    }

    fun setOnClick(){

        orderAddressAnother.setOnTouchListener(object : View.OnTouchListener{
            override fun onTouch(view: View?, event: MotionEvent?): Boolean {
                if(event?.action == MotionEvent.ACTION_DOWN){
                    val intent = Intent(this@OrderActivity, DaumAddressApiActivity::class.java)
                    startActivityForResult(intent,1)
                }
                return false
            }
        })
        checkBox_Address.setOnCheckedChangeListener(object : CompoundButton.OnCheckedChangeListener{
            override fun onCheckedChanged(p0: CompoundButton?, checked: Boolean) {
                if(checked){
                    order_address_another.visibility = View.GONE
                    order_address_member.visibility = View.VISIBLE
                } else {
                    order_address_member.visibility = View.GONE
                    order_address_another.visibility = View.VISIBLE
                }
            }
        })
    }

    fun getOrderList(){
        val call = Common.api.getCartList(email = appData.getString("email",""))
        call.enqueue(object : Callback<List<CartListResponse>>{
            override fun onResponse(call: Call<List<CartListResponse>>?, response: Response<List<CartListResponse>>?) {
                orderList = response?.body()!!
                orderlistRecyclerView.adapter = OrderAdapter(orderList,this@OrderActivity)
                orderlistRecyclerView.adapter.notifyDataSetChanged()
                orderTotalPrice.text = TextUtil.setPrice((orderlistRecyclerView.adapter as OrderAdapter).getTotalPrice().toString()) + "원"

            }
            override fun onFailure(call: Call<List<CartListResponse>>?, t: Throwable?) {

            }

        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        super.onActivityResult(requestCode, resultCode, intent)
        if (requestCode ==1){
            if(resultCode == Activity.RESULT_OK){
                val extras : Bundle = intent?.extras!!
                if(extras !=null){
                    orderAddressAnother.setText(extras.getString("result"))
                }
            }
        }
    }
}
