package com.erang.rangshop.api.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class LoginResponse{
    @SerializedName("result")
    @Expose
    var result : String?=null

    @SerializedName("name")
    @Expose
    var name : String?=null

    @SerializedName("address")
    @Expose
    var address : String?=null

    @SerializedName("address_detail")
    @Expose
    var address_detail : String?=null
}