package com.erang.rangshop.api

import com.erang.rangshop.api.response.*
import okhttp3.ResponseBody

import retrofit2.Call
import retrofit2.http.*

interface APIInterface {
    @FormUrlEncoded
    @POST("user/register.php")
    fun register(@Field("email") email: String,
                 @Field("name") name: String,
                 @Field("password") password: String,
                 @Field("address") address: String,
                 @Field("address_detail")address_detail : String): Call<ResponseBody>

    @FormUrlEncoded
    @POST("user/login.php")
    fun login(@Field("email") email: String,
              @Field("password") password: String): Call<LoginResponse>

    @FormUrlEncoded
    @POST("user/doLike.php")
    fun doLike(@Field("email")email : String,
               @Field("product_num")product_num: String) : Call<ResponseBody>


    @FormUrlEncoded
    @POST("user/deleteCart.php")
    fun deleteCart(@Field("cart_idx[]") cart_idx : ArrayList<String?>,
                   @Field("email")email:String,
                   @Field("size")size:String) : Call<ResultResponse>

    @GET("user/listSelling.php")
    fun getSellingList(@Query("category") category: String) :Call<List<SellingListResponse>>

    @GET("user/listLikes.php")
    fun getLikesList(@Query("category") category: String,
                     @Query("email") email:String) :Call<List<LikesListResponse>>

    @GET("user/listCart.php")
    fun getCartList(@Query("email")email:String) : Call<List<CartListResponse>>


    @GET("user/addCart.php")
    fun addCart(@Query("email")email:String ,@Query("product_num") product_num: String) : Call<ResponseBody>

    @GET("management/product/productView.php")
    fun getProduct(@Query("product_num") product_num: String,
                   @Query("isLogin") isLogin: Boolean,
                   @Query("email") email :String) :Call<List<ProductResponse>>
}
