package com.erang.rangshop.fragment

import android.app.Fragment
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.erang.rangshop.R
import com.erang.rangshop.adpater.LikesAdapter
import com.erang.rangshop.api.response.LikesListResponse
import com.erang.rangshop.util.Common
import kotlinx.android.synthetic.main.fragment_likes.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class LikesFragment : Fragment() {

    lateinit var likesList : List<LikesListResponse>
    lateinit var likesRecyclerView : RecyclerView
    lateinit var likesView : View
    lateinit var appData : SharedPreferences
    lateinit var menuTab : TabLayout

    lateinit var email : String
    val category =arrayOf("전체","반지","목걸이","귀걸이","팔찌")


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        likesView =inflater.inflate(R.layout.fragment_likes, container, false)

        init()
        setOnClick()
        getData(category[0])

        return likesView
    }

    fun init(){
        appData = activity.getSharedPreferences("appData",MODE_PRIVATE)
        email = appData.getString("email","")

        likesList = ArrayList()
        likesRecyclerView = likesView.likesList
        likesRecyclerView.layoutManager = LinearLayoutManager(activity)
        menuTab = likesView.likes_list_tab

    }

    fun setOnClick(){
        menuTab.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener{
            override fun onTabReselected(tab: TabLayout.Tab?) {
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                when(tab?.position){
                    0 -> getData(category[0])
                    1 -> getData(category[1])
                    2 -> getData(category[2])
                    3 -> getData(category[3])
                    4 -> getData(category[4])
                }
            }

        })

    }

    fun getData(category: String){
        val call = Common.api.getLikesList(category = category, email = email)
        call.enqueue(object : Callback<List<LikesListResponse>> {
            override fun onResponse(call: Call<List<LikesListResponse>>, response: Response<List<LikesListResponse>>?) {
                likesList = response?.body()!!
                likesRecyclerView.adapter = LikesAdapter(likesList,activity)
                likesRecyclerView.adapter.notifyDataSetChanged()

            }
            override fun onFailure(call: Call<List<LikesListResponse>>?, t: Throwable?) {

            }
        })
    }

    override fun onResume() {
        super.onResume()
        getData(category[0])
    }



}