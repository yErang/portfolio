package com.erang.rangshop.fragment

import android.app.Fragment
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.erang.rangshop.R
import com.erang.rangshop.adpater.SellingAdapter
import com.erang.rangshop.api.response.SellingListResponse
import com.erang.rangshop.util.Common
import kotlinx.android.synthetic.main.fragment_selling.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.ArrayList

/* 상품 목록 페이지 */
class SellingFragment : Fragment() {

    lateinit var sellingList : List<SellingListResponse>
    lateinit var sellingView : View
    lateinit var sellingRecyclerView : RecyclerView

    lateinit var menuTab : TabLayout
    val category =arrayOf("전체","반지","목걸이","귀걸이","팔찌")

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        sellingView = inflater.inflate(R.layout.fragment_selling, container, false)

        init()
        setOnClick()

        return sellingView
    }

    fun init(){
        sellingList = ArrayList()
        sellingRecyclerView = sellingView.sellList
        sellingRecyclerView.layoutManager = LinearLayoutManager(activity)
        menuTab = sellingView.sell_list_tab
    }

    fun setOnClick(){
        menuTab.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener{
            override fun onTabReselected(tab: TabLayout.Tab?) {
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                when(tab?.position){
                    0 -> getData(category[0])
                    1 -> getData(category[1])
                    2 -> getData(category[2])
                    3 -> getData(category[3])
                    4 -> getData(category[4])
                }
            }

        })

    }

    fun getData(category: String){
        val call = Common.api.getSellingList(category = category)
        call.enqueue(object : Callback<List<SellingListResponse>> {
            override fun onResponse(call: Call<List<SellingListResponse>>, response: Response<List<SellingListResponse>>?) {
                sellingList = response?.body()!!
                sellingRecyclerView.adapter = SellingAdapter(sellingList,activity)
                sellingRecyclerView.adapter.notifyDataSetChanged()
            }
            override fun onFailure(call: Call<List<SellingListResponse>>?, t: Throwable?) {
            }

        })
    }

    override fun onResume() {
        super.onResume()
        getData(category[0])
    }
}
