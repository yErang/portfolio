package com.erang.rangshop.fragment


import android.app.Fragment
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast

import com.erang.rangshop.R
import com.erang.rangshop.activity.BuyListActivity
import com.erang.rangshop.activity.UpdateInfoActivity
import kotlinx.android.synthetic.main.fragment_mypage.view.*



class MyPageFragment : Fragment() {
    lateinit var nameText : TextView
    lateinit var btnUpdateInfo : Button
    lateinit var btnBuyList : Button
    lateinit var btnLogout : Button
    lateinit var fragmentShopping : Fragment
    lateinit var login : SharedPreferences
    lateinit var mypageView : View

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        mypageView= inflater.inflate(R.layout.fragment_mypage, container, false)

        init()
        setOnClick()

        return mypageView
    }

    fun init(){
        nameText = mypageView.mypage_name
        btnUpdateInfo = mypageView.mypage_update_btn
        btnBuyList = mypageView.mypage_buy_btn
        btnLogout = mypageView.mypage_logout_btn
        login = activity.getSharedPreferences("appData",Context.MODE_PRIVATE)
        nameText.text = login.getString("name","")+"님의 마이페이지"
        fragmentShopping = SellingFragment()
    }

    fun setOnClick(){
        btnLogout.setOnClickListener {
            val editor = login.edit()
            editor.clear()
            editor.putBoolean("isLogin",false)
            editor.apply()
           fragmentManager.beginTransaction().replace(R.id.main_content,fragmentShopping).commit()
            Toast.makeText(activity,"로그아웃 되었습니다.",Toast.LENGTH_SHORT).show()
        }
        btnBuyList.setOnClickListener {
            val intent = Intent(activity,BuyListActivity::class.java)
            startActivity(intent)
        }
        btnUpdateInfo.setOnClickListener {
            val intent = Intent(activity,UpdateInfoActivity::class.java)
            startActivity(intent)
        }
    }


}
