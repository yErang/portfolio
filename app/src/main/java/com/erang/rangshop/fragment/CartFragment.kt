package com.erang.rangshop.fragment


import android.app.AlertDialog
import android.os.Bundle
import android.app.Fragment
import android.content.Context.MODE_PRIVATE
import android.content.DialogInterface
import android.content.Intent
import android.content.SharedPreferences
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*

import com.erang.rangshop.R
import com.erang.rangshop.activity.OrderActivity
import com.erang.rangshop.adpater.CartAdapter
import com.erang.rangshop.api.response.CartListResponse
import com.erang.rangshop.api.response.ResultResponse
import com.erang.rangshop.util.Common
import com.erang.rangshop.util.TextUtil
import kotlinx.android.synthetic.main.fragment_cart.view.*
import okhttp3.MediaType
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class CartFragment : Fragment() {
    lateinit var cartView : View
    lateinit var cartList : List<CartListResponse>
    lateinit var cartRecyclerView : RecyclerView
    lateinit var checkedItems : ArrayList<String?>
    lateinit var cartRemove : ImageButton

    lateinit var cartSelectAll : CheckBox
    lateinit var cartOrderDo : Button
    lateinit var cartTotalPrice : TextView
    lateinit var cartPriceLayout : LinearLayout


    lateinit var appData : SharedPreferences
    lateinit var email : String
    var cartSize : Int = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        cartView =  inflater.inflate(R.layout.fragment_cart, container, false)


        init()
        setOnClick()
        getCartList()
        return cartView
    }

    fun init(){

        appData = activity.getSharedPreferences("appData",MODE_PRIVATE)
        email = appData.getString("email","")

        cartList = ArrayList()
        cartSelectAll = cartView.cart_select_all
        cartRemove = cartView.cart_remove
        cartRecyclerView = cartView.cart_list
        cartRecyclerView.adapter = CartAdapter(cartList,activity)
        cartRecyclerView.layoutManager = LinearLayoutManager(activity)
        cartOrderDo = cartView.cart_order_do
        cartTotalPrice = cartView.cart_totalprice
        cartPriceLayout = cartView.cart_pricelayout


    }

    fun setOnClick(){
        cartSelectAll.setOnCheckedChangeListener { buttonView, isChecked ->
            if(isChecked){
                (cartRecyclerView.adapter as CartAdapter).selectAll()
            } else {
                (cartRecyclerView.adapter as CartAdapter).clearAll()
            }
        }
        cartOrderDo.setOnClickListener {
            cartSize = (cartRecyclerView.adapter as CartAdapter).itemCount
            if(cartSize!=0){
                val intent = Intent(activity,OrderActivity::class.java)
                startActivity(intent)
            } else {
                Toast.makeText(activity,"주문할 상품이 없습니다.",Toast.LENGTH_SHORT).show()
            }
        }
        cartRemove.setOnClickListener {
             checkedItems = (cartRecyclerView.adapter as CartAdapter).getCheckedItems()
                if (checkedItems.isNotEmpty()){
                    val dialog = AlertDialog.Builder(activity)
                    dialog.setTitle("선택한 상품을 삭제하시겠습니까?")
                    dialog.setPositiveButton("확인") { dialog, which ->
                        doRemove()
                        cartSelectAll.isChecked = false
                    }
                    dialog.setNegativeButton("취소") { dialog, which ->
                        dialog.dismiss()
                    }
                    dialog.show()
                }else if(checkedItems.isEmpty()){
                    Toast.makeText(activity,"삭제할 항목을 체크 해 주세요.",Toast.LENGTH_SHORT).show()
                }

        }
    }

    fun getCartList(){
        val call = Common.api.getCartList(email = email)
        call.enqueue(object : Callback<List<CartListResponse>> {
            override fun onResponse(call: Call<List<CartListResponse>>?, response: Response<List<CartListResponse>>?) {
                cartList = response?.body()!!
                cartRecyclerView.adapter = CartAdapter(cartList,activity)
                cartRecyclerView.adapter.notifyDataSetChanged()
                cartTotalPrice.text = TextUtil.setPrice((cartRecyclerView.adapter as CartAdapter).getTotalPrice().toString()) + "원"
            }
            override fun onFailure(call: Call<List<CartListResponse>>?, t: Throwable?) {

            }

        })
    }

    fun doRemove(){
        val call = Common.api.deleteCart(cart_idx = checkedItems,email = email,size = checkedItems.size.toString())

        call.enqueue(object : Callback<ResultResponse>{
            override fun onResponse(call: Call<ResultResponse>, response: Response<ResultResponse>) {
                if(response.body()!!.success){
                   getCartList()
                } else {
                    Log.v("result",response.body()!!.message)
                }
            }
            override fun onFailure(call: Call<ResultResponse>, t: Throwable) {
            }

        })
    }


}
