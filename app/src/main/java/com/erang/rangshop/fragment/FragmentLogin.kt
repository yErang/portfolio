package com.erang.rangshop.fragment

import android.app.Fragment
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.erang.rangshop.R
import com.erang.rangshop.activity.RegisterActivity
import com.erang.rangshop.api.APIInterface
import com.erang.rangshop.api.response.LoginResponse
import com.erang.rangshop.util.Common
import com.erang.rangshop.util.TextUtil
import kotlinx.android.synthetic.main.fragment_login.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class FragmentLogin : Fragment() {
    lateinit var textEmail : EditText
    lateinit var textPassword : EditText
    lateinit var btnRegister : Button
    lateinit var btnLogin : Button
    lateinit var apiInterface : APIInterface
    lateinit var fragmentMypage : Fragment
    lateinit var loginView : View

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        loginView = inflater.inflate(R.layout.fragment_login, container, false)

        init()
        setOnClick()

        return loginView
    }
    fun init(){
        textEmail = loginView.login_email_input
        textPassword = loginView.login_password_input
        btnRegister = loginView.login_register_btn
        btnLogin = loginView.login_login_btn
        apiInterface = Common.api
        fragmentMypage = MyPageFragment()
    }

    fun setOnClick(){
        btnRegister.setOnClickListener{
            val intentRegister = Intent(activity, RegisterActivity::class.java)
            startActivity(intentRegister)
        }
        btnLogin.setOnClickListener{
            if(TextUtil.isEmpty(textEmail)||TextUtil.isEmpty(textPassword)){
                Toast.makeText(activity,"빈 칸을 입력 해 주세요.",Toast.LENGTH_SHORT).show()

                if(TextUtil.isEmpty(textEmail)){
                    TextUtil.requestFocus(textEmail)
                }else{
                    TextUtil.requestFocus(textPassword)
                }

            } else if(!TextUtil.emailValidate(textEmail)){
                Toast.makeText(activity,"올바른 이메일 형식이 아닙니다.",Toast.LENGTH_SHORT).show()
                textEmail.requestFocus()
            } else {
                login()
            }
        }
    }

    fun login(){
        val call:Call<LoginResponse> = apiInterface.login(textEmail.text.toString(),textPassword.text.toString())
        call.enqueue(object: Callback<LoginResponse>{
            override fun onResponse(call: Call<LoginResponse>, response: Response<LoginResponse>) {
                val result = response.body()?.result
                val name = response.body()?.name
                val address = response.body()?.address
                val address_detail = response.body()?.address_detail
                when(result){
                    "not registered" ->{
                        Toast.makeText(activity,"등록되지 않은 이메일 주소입니다",Toast.LENGTH_SHORT).show()
                    }
                    "password error"->{
                        Toast.makeText(activity,"비밀번호를 확인 해 주세요",Toast.LENGTH_SHORT).show()
                    }
                    "success"->{
                        Toast.makeText(activity,"로그인이 완료되었습니다.",Toast.LENGTH_SHORT).show()
                        val login = activity.getSharedPreferences("appData", Context.MODE_PRIVATE)
                        var editor : SharedPreferences.Editor = login.edit()
                        editor.putString("email",textEmail.text.toString())
                        editor.putString("name",name)
                        editor.putString("address",address)
                        editor.putString("address_detail",address_detail)
                        editor.putBoolean("isLogin",true)
                        editor.apply()
                        fragmentManager.beginTransaction().replace(R.id.main_content,fragmentMypage).commit()
                    }
                }
            }
            override fun onFailure(call: Call<LoginResponse>?, t: Throwable?) {
                Toast.makeText(activity,t!!.message,Toast.LENGTH_SHORT).show()
            }

        })
    }
}
