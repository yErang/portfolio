package com.erang.rangshop.adpater

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.erang.rangshop.R
import com.erang.rangshop.activity.ProductViewActivity
import com.erang.rangshop.api.response.LikesListResponse
import com.erang.rangshop.util.TextUtil
import kotlinx.android.synthetic.main.fragment_likes_item.view.*

class LikesAdapter(var items : List<LikesListResponse>, val context: Context) : RecyclerView.Adapter<LikesAdapter.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.fragment_likes_item, parent, false))

    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        Glide.with(context).load(items[position].img).thumbnail(0.1f).apply(RequestOptions.noTransformation()).into(holder.likes_productImg)
        holder.likes_productName.text = items[position].name
        holder.likes_productPrice.text = TextUtil.setPrice(items[position].price) +"원"
        if(items[position].quantity?.toInt() == 0){
            holder.likes_productSoldout.visibility = View.VISIBLE
        } else {
            holder.likes_productSoldout.visibility = View.GONE
        }

        holder.likes_productPurity.text = items[position].purity
        val productNum = items[position].num
        holder.itemView.setOnClickListener {
            val intent = Intent(context, ProductViewActivity::class.java)
            intent.putExtra("product_num",productNum)
            context.startActivity(intent)
        }
    }

    class ViewHolder (view: View) : RecyclerView.ViewHolder(view) {
        val likes_productImg = view.likes_list_img
        val likes_productSoldout = view.likes_list_soldout
        val likes_productName = view.likes_list_name
        val likes_productPrice = view.likes_list_price
        val likes_productPurity = view.likes_list_purity
    }
}