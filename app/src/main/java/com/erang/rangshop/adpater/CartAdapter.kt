package com.erang.rangshop.adpater

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.erang.rangshop.R
import com.erang.rangshop.activity.ProductViewActivity
import com.erang.rangshop.api.response.CartListResponse
import com.erang.rangshop.util.TextUtil
import kotlinx.android.synthetic.main.fragment_cart_item.view.*

class CartAdapter(val items : List<CartListResponse>, val context:Context) : RecyclerView.Adapter<CartAdapter.ViewHolder>() {

    var checkedList : ArrayList<String?> = ArrayList()
    var isSelectAll : Boolean = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CartAdapter.ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.fragment_cart_item, parent, false))
    }

    override fun getItemCount(): Int {
       return items.size
    }

    override fun onBindViewHolder(holder: CartAdapter.ViewHolder, position: Int) {
        Glide.with(context).load(items[position].img).thumbnail(0.1f).apply(RequestOptions.noTransformation()).into(holder.cart_productImg)
        holder.cart_productName.text = items[position].name
        holder.cart_productPrice.text = TextUtil.setPrice(items[position].price) +"원"

        holder.cart_productPurity.text = items[position].purity
        val productNum = items[position].num
        holder.itemView.setOnClickListener {
            val intent = Intent(context, ProductViewActivity::class.java)
            intent.putExtra("product_num",productNum)
            context.startActivity(intent)
        }

        if(items[position].category=="반지"){
            holder.cart_ringSize.visibility = View.VISIBLE
        } else {
            holder.cart_ringSize.visibility = View.GONE
        }

        holder.cart_checkbox.setOnCheckedChangeListener { buttonView, isChecked ->
            if(isChecked){
                checkedList.add(items[position].cartIndex)
            }else{
                checkedList.remove(items[position].cartIndex)
            }
        }
        holder.cart_checkbox.isChecked = isSelectAll
        holder.cart_Index = items[position].cartIndex
    }


    class ViewHolder(view: View) : RecyclerView.ViewHolder(view){
        val cart_productImg = view.cart_list_img
        val cart_productName = view.cart_list_name
        val cart_productPrice = view.cart_list_price
        val cart_productPurity = view.cart_list_purity
        val cart_ringSize = view.cart_size
        val cart_checkbox = view.cart_select
        var cart_Index : String?=null

    }

    fun getTotalPrice() : Int?{

        var totalPrice  = 0
        if(items.isNotEmpty()){
            for (position in 0 until items.size){
                totalPrice += items[position].price!!.toInt()
            }
        }
        return totalPrice
    }

    fun getCheckedItems(): ArrayList<String?> {
        return checkedList
    }

    fun selectAll() {
        isSelectAll = true
        notifyDataSetChanged()
    }

    fun clearAll(){
        isSelectAll = false
        notifyDataSetChanged()
    }




}