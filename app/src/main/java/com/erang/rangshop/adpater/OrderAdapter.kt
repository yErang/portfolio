package com.erang.rangshop.adpater

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.erang.rangshop.R
import com.erang.rangshop.activity.ProductViewActivity
import com.erang.rangshop.api.response.CartListResponse

import com.erang.rangshop.api.response.OrderListResponse
import com.erang.rangshop.util.TextUtil
import kotlinx.android.synthetic.main.activity_order_item.view.*
import kotlinx.android.synthetic.main.fragment_likes_item.view.*

class OrderAdapter(var items : List<CartListResponse>, val context: Context) : RecyclerView.Adapter<OrderAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.activity_order_item, parent, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        Glide.with(context).load(items[position].img).thumbnail(0.1f).apply(RequestOptions.noTransformation()).into(holder.order_productImg)
        holder.order_productName.text = items[position].name
        holder.order_productPrice.text = TextUtil.setPrice(items[position].price) +"원"

        holder.order_productPurity.text = items[position].purity
        val productNum = items[position].num
        holder.itemView.setOnClickListener {
            val intent = Intent(context, ProductViewActivity::class.java)
            intent.putExtra("product_num",productNum)
            context.startActivity(intent)
        }

        if(items[position].category=="반지"){
            holder.order_productSize.visibility = View.VISIBLE
        } else {
            holder.order_productSize.visibility = View.GONE
        }
    }


    class ViewHolder(view : View): RecyclerView.ViewHolder(view){
        val order_productImg = view.order_list_img
        val order_productName = view.order_list_name
        val order_productPrice = view.order_list_price
        val order_productPurity = view.order_list_purity
        val order_productSize = view.order_size
    }

    fun getTotalPrice() : Int?{

        var totalPrice  = 0
        if(items.isNotEmpty()){
            for (position in 0 until items.size){
                totalPrice += items[position].price!!.toInt()
            }
        }
        return totalPrice
    }
}